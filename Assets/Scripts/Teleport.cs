﻿using System;
using System.Collections;
using UnityEngine;

public class Teleport : MonoBehaviour{
    [SerializeField] private GameObject       m_Teleport2;
    private                  PlayerController _playerController;

    private bool _portalPause = true;

    private void Start() {
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(other.gameObject.transform.name.Equals("Player")) {
            if(Input.GetKeyDown(KeyCode.W) && _portalPause) {
                _playerController.IsStopInput = true;
                _portalPause                  = false;
                StartCoroutine(TeleportStation(other.gameObject));
            }
        }
    }

    private IEnumerator TeleportStation(GameObject obj) {
      
        yield return new WaitForSeconds(2f);
        obj.transform.position         = m_Teleport2.transform.position;
        _playerController.IsStopInput = false;
        _portalPause = true;
    }
}