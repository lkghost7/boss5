﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerToEnemy : MonoBehaviour
{
    [SerializeField] private GameObject _Player;
    [SerializeField] private GameObject _Enemy;
    
    
    
    // Update is called once per frame
    void Update() {

        _Enemy.transform.position =
            Vector2.MoveTowards
                (_Enemy.transform.position,
                 _Player.transform.position,
                 Time.deltaTime * 7f);
    }
}
