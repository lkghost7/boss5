﻿using System;
using System.Collections;
using UnityEngine;

public class Teleport2 : MonoBehaviour{
    [SerializeField] private GameObject       m_Teleport1;
    private                  bool             _isTeleportReady;
    private                  PlayerController _playerController;

    private bool _portalPause = true;


    private void Start() {
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerStay2D(Collider2D other) {
        if(Input.GetKeyDown(KeyCode.W) && _portalPause) {
            _playerController.IsStopInput = true;
            _portalPause                  = false;
            StartCoroutine(TeleportStation(other.gameObject));
        }
    }

    private IEnumerator TeleportStation(GameObject obj) {
        yield return new WaitForSeconds(2f);
        obj.transform.position        = m_Teleport1.transform.position;
        _playerController.IsStopInput = false;
        _portalPause                  = true;
    }
}
