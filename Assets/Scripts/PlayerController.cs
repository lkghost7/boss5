﻿using UnityEngine;

public class PlayerController : MonoBehaviour{
    private       Rigidbody2D _rb;
    private       float       _horizontal;
    private       Vector2     _direction;
    private const float       MAX_SPEED = 5;
    private const            float      MAX_JUMP = 15;
    [SerializeField] private GameObject _point;
    [SerializeField] private GameObject _shootPoint;
    

    public bool IsStopInput {get; set;}

    private Ladder2 _ladder2;
    
    private                  bool       isGrounded;

    private void Start() {
        _rb         = GetComponent<Rigidbody2D>();
        _horizontal = 0;
       
    }

    private bool IsGround() {
        Vector2      point = _point.transform.position;
        RaycastHit2D _hit  = Physics2D.Raycast(point, Vector2.down, 0.2f);
        return _hit.collider != null;
    }

    private void ChekEnemyHitBox() {
        Vector2 shootPoint = _shootPoint.transform.position;
        RaycastHit2D _raycast = Physics2D.Raycast(shootPoint, Vector2.right, 0.2f);
        if(_raycast.collider != null) {
            
        }
    }

    private void ChekEnemyHitBox2() {
        
        Vector2      _shooting = _shootPoint.transform.position;
        RaycastHit2D rayhit    = Physics2D.Raycast(_shooting, Vector2.right, 0.2f);
        if(rayhit.collider != null)
        {
            print(rayhit.transform.gameObject.name);
           
        }
    }

    private void Update() {
        
        if(IsStopInput) {
            _rb.velocity = Vector2.zero;
            return;
        }
        
        _ladder2 = FindObjectOfType<Ladder2>();
       
            _horizontal = Input.GetAxis("Horizontal");
            _direction = new Vector2(_horizontal, 0f);
            isGrounded = IsGround();
        ChekEnemyHitBox();
        Move(_direction);
      

        if((Input.GetKeyDown(KeyCode.Space)) 
        && (isGrounded) 
        
        && (!IsStopInput) ){ Jump(); }
      }

    public void Move(Vector2 direction) {
        _rb.velocity = new Vector2(direction.x * MAX_SPEED, _rb.velocity.y);
    }

    public void Jump() {
        _rb.AddForce(Vector2.up * MAX_JUMP, ForceMode2D.Impulse);
    }
}