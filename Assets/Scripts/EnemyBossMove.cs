﻿using System.Collections;
using UnityEngine;

enum EnemyState{
    IDLE,
    ATTACK1,
    ATTACK2
}

public class EnemyBossMove : MonoBehaviour{
    [SerializeField] private GameObject _Hero;
    [SerializeField] private GameObject _FireBall;
    [SerializeField] private GameObject _EnemyPosition;
    [SerializeField] private GameObject _mageShootPoint;
    [SerializeField] private GameObject _startPointEnemy;
    [SerializeField] private GameObject _portalPosition;

    private float speedFireBall = 2f;
    private bool  isFlag;

    private Animator _animator;

    private EnemyState _state {
        get {return(EnemyState) _animator.GetInteger("State");}
        set {_animator.SetInteger("State", (int) value);}
    }

    void Start() {
        _animator = GetComponent<Animator>();
    }

    void Update() {
        
  EnemyStrikeFireBallToPlayer();

        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            _state = EnemyState.IDLE;
            Vector2 enemy  = _startPointEnemy.transform.position;
            Vector2 player = _Hero.transform.position;
            enemy = Vector2.MoveTowards(enemy, player, Time.deltaTime);
        }

        if(Input.GetKeyDown(KeyCode.Alpha2)) {
            _state = EnemyState.ATTACK1;
            FirballStrikeEnemy();
        }

        if(Input.GetKeyDown(KeyCode.Alpha3)) {
            _state = EnemyState.ATTACK2;
        }
    }

    private void FirballStrikeEnemy() {
        GameObject  fireBall = CreateFireBall();
        Rigidbody2D _rbFire  = fireBall.GetComponent<Rigidbody2D>();

        Vector2 shootPointPos = _EnemyPosition.transform.position;
        Vector2 herPos        = _Hero.transform.position;
        StartCoroutine(MoveFireBall(shootPointPos, herPos, _rbFire));
    }

    // private IEnumerator FireBallStrikeHomingMissile(GameObject player) {
    //     GameObject fireBall = CreateFireBall();
    //     Vector2 enemy = _EnemyPosition.transform.position;
    //     Vector2 playerTarget = player.transform.position;
    //     fireBall.transform.position =  Vector2.MoveTowards(enemy, playerTarget, Time.deltaTime *2f);
    //     yield return null;
    // }

    private IEnumerator FireBallStrikeHomingMissile(GameObject fireBall) {
       
        Vector2 playerTarget = _Hero.transform.position;
       
        while(true) {
           
            yield return null;
            fireBall.transform.position = Vector2.MoveTowards(fireBall.transform.position, playerTarget, Time.deltaTime * 
            2f);
        }
    }

    private IEnumerator MoveFireBall(Vector2 shootPointPos, Vector2 herPos, Rigidbody2D _rbFire) {
        Vector2 velocity = CalculateVelocity(shootPointPos, herPos);
        _rbFire.velocity = velocity;
        yield break;
    }

    private void EnemyStrikeFireBallToPlayer() {
        if(isFlag) {
            return;
        }

        Vector2 enemypos      = _mageShootPoint.transform.position;
        Vector2 shootPointPos = _EnemyPosition.transform.position;
        Vector2 herPos        = _Hero.transform.position;

        RaycastHit2D ray = Physics2D.Raycast(enemypos, Vector2.left, 100f);

        if(ray.collider != null) {
            // print(ray.transform.gameObject.name);
            if(ray.transform.gameObject.name.Equals("Player")) {
                // GameObject obj = ray.transform.gameObject;
                GameObject fireball = CreateFireBall();

                // FirballStrikeEnemy();
                StartCoroutine(FireBallStrikeHomingMissile(fireball));
                isFlag = true;
            }
        }
    }

    private Vector2 CalculateVelocity(Vector2 ShootPointPos, Vector2 herPos) {
        float velocityX = (herPos.x - ShootPointPos.x) / speedFireBall;
        float velocityY = (herPos.y - ShootPointPos.y) / speedFireBall;
        return new Vector2(velocityX, velocityY);
    }

    private GameObject CreateFireBall() {
        GameObject inst = Instantiate(_FireBall);
        inst.transform.position = _EnemyPosition.transform.position;
        return inst;
    }
}
