﻿using System;
using UnityEngine;

public enum EEnemyState{
    WalkToPlayer,
    LadderMove,
    Stay,
    PickUp
}

public class MoveEnemy : MonoBehaviour{
    [SerializeField] private GameObject m_Player;
    [SerializeField] private GameObject m_Ladder;
    [SerializeField] private GameObject m_Zombie;

    private float          SpeedEnemy = 0.05f;
    private Rigidbody2D    _rbZombie;
    private SpriteRenderer _spriteRenderer;
    private EEnemyState    _currentState = EEnemyState.WalkToPlayer;
    private float _delayTime;
    

    public bool IsStopEnemyMove     {get; set;}
    public bool IsLadderMove        {get; set;}
    public bool IsCenterEnemyLadder {get; set;}

    private void Start() {
        _rbZombie           = GetComponent<Rigidbody2D>();
        _spriteRenderer     = GetComponentInChildren<SpriteRenderer>();
        IsStopEnemyMove     = false;
        IsLadderMove        = false;
        IsCenterEnemyLadder = false;
    }

    // Update Zone --------------------------------------------------------------- Update Zone \\
    private void FixedUpdate() {
        switch(_currentState) {
            case EEnemyState.WalkToPlayer :
                MoveEnemyToPlayerAxisX();
                break;
            case EEnemyState.LadderMove :
                LadderMove();
                break;
            case EEnemyState.Stay :   break;
            case EEnemyState.PickUp : break;
            default :                 throw new ArgumentOutOfRangeException();
        }
    }

    // Update Zone ---------------------------------------------------------------- Update Zone \\
    // Trigger Zone -------------------------------------------------------------- Trigger Zone \\

    private void OnTriggerEnter2D(Collider2D other) {
        if(Time.time > _delayTime) {
            if(other.gameObject.transform.name.Equals("Ladder")) {
                CenterLadder();
            }    
        }
    }

    public void OnTriggerStay2D(Collider2D other) {
        if(other.gameObject.transform.name.Equals("Ladder")) {
            IsStopEnemyMove = true;
            LadderMove();
        }
    }

    public void OnTriggerExit2D(Collider2D other) {
        if(other.transform.gameObject.name.Equals("Ladder")) {
            IsStopEnemyMove     = false;
            _delayTime = Time.time + 1f;
            IsCenterEnemyLadder = false;

            _rbZombie.gravityScale = 1;
        }
    }

    // Trigger Zone -------------------------------------------------------------- Trigger Zone \\
    // Method Zone ---------------------------------------------------------------- Method Zone \\
    private void LadderMove() {
        if(IsLadderMove) {
            return;
        }

        var zombiObj   = gameObject;
        var _zombiePos = gameObject.transform.position;
        var _rbZombie  = gameObject.GetComponent<Rigidbody2D>();
        _rbZombie.gravityScale = 0;

        if(m_Player.transform.position.y >= _zombiePos.y) {
            _rbZombie.velocity  = Vector2.up * 3f;
            IsCenterEnemyLadder = false;
        }
        else if((m_Player.transform.position.y <= _zombiePos.y)) {
            _rbZombie.velocity = Vector2.down * 3f;
        }
    }

    public void CenterLadder() {
        if(IsCenterEnemyLadder) {
            return;
        }

        m_Zombie.transform.position = new Vector2(m_Ladder.transform.position.x, m_Zombie.transform.position.y);
        IsCenterEnemyLadder         = true;
    }

    public void MoveEnemyToPlayerAxisX() {
        if(IsStopEnemyMove) {
            return;
        }

        Vector2 playerPos = m_Player.transform.position;

        if(playerPos.x > transform.position.x) {
            transform.position    += transform.right * SpeedEnemy;
            _spriteRenderer.flipX =  true;
        }

        else {
            transform.position    += transform.right * -SpeedEnemy;
            _spriteRenderer.flipX =  false;
        }
    }

    // Method Zone ---------------------------------------------------------------- Method Zone \\
}
