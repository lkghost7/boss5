﻿using System;
using UnityEngine;

public class Ladder2 : MonoBehaviour{
    private GameObject  _player;
    private Rigidbody2D _rbPlayer;

    private GameObject  _zombie;
    private Rigidbody2D _rbZombie;

    private MoveEnemy _moveEnemy;

    public bool PlayerIsLadder {get; private set;}

    private void Start() {
        _moveEnemy = FindObjectOfType<MoveEnemy>();
     
    }

    public void OnTriggerStay2D(Collider2D other) {
        string nameObj = other.gameObject.transform.name;
        Debug.Log("______" + nameObj);
        if(nameObj.Equals("Player")) {
            _player                = other.gameObject;
            PlayerIsLadder         = true;
            _rbPlayer              = _player.GetComponent<Rigidbody2D>();
            _rbPlayer.gravityScale = 0;
            _rbPlayer.velocity     = Vector2.zero;
            Debug.Log("player _");
        }

        if(nameObj.Equals("EnemyZomby")) {
            _zombie                = other.gameObject;
            _rbZombie              = _zombie.GetComponent<Rigidbody2D>();
            _rbZombie.gravityScale = 0;
            if(_player.transform.position.y >= _zombie.transform.position.y) {
                
                _rbZombie.velocity = Vector2.up;
                _moveEnemy.IsStopEnemyMove = true;
            } else if((_player.transform.position.y <= _zombie.transform.position.y)) {
                _rbZombie.velocity         = Vector2.down;
                _moveEnemy.IsStopEnemyMove = true;
            }
            
        }
    }

    public void OnTriggerExit2D(Collider2D other) {
        string nameObj = other.gameObject.transform.name;
        PlayerIsLadder = false;
        if(nameObj.Equals("Player")) {
            _rbPlayer.gravityScale = 1;
        }

        if(nameObj.Equals("EnemyZomby")) {
            _rbZombie.gravityScale = 1;
        }
    }

    private void Update() {
        if(!PlayerIsLadder) {
            return;
        }

        if(Input.GetKey(KeyCode.W)) {
            _rbPlayer.velocity = new Vector2(0, 4);
        }

        if(Input.GetKeyUp(KeyCode.W)) {
            _rbPlayer.velocity = new Vector2(0, 0);
        }

        if(Input.GetKey(KeyCode.S)) {
            _rbPlayer.velocity = new Vector2(0, -4);
        }

        if(Input.GetKeyUp(KeyCode.S)) {
            ;
            _rbPlayer.velocity = new Vector2(0, 0);
        }
    }
}