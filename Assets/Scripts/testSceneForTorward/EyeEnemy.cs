﻿using System;
using System.Collections;
using UnityEngine;

public class EyeEnemy : MonoBehaviour{
    [SerializeField] private GameObject _Player;
    [SerializeField] private GameObject _Enemy;
    [SerializeField] private GameObject _ShootRay;
    [SerializeField] private GameObject _Portal;

    private bool _strCarutune = true;
    private bool _testDebug = true;

    private Rigidbody2D _rb;
    private IEnumerator _coroutine;
    private Coroutine _portalCoroutine;
    private float speedPortal =3f;

    
    public void Start() {
        _rb = GetComponent<Rigidbody2D>();
        _coroutine = MovePortal();
        _portalCoroutine = StartCoroutine(_coroutine);
    }

    void Update() {
        if(! _strCarutune) {
            _testDebug = false;
        } 
    }

 
    private void OnTriggerEnter2D(Collider2D other) {
        
        Debug.Log(other.gameObject.transform.name  +" ----");
        if(other.gameObject.transform.name.Equals("portal")) {
            Debug.Log("trigerred");
            // _strCarutune = false;
            StopCoroutine(_portalCoroutine);
            if(other != null) {
                Destroy(other.gameObject);
            }
        }
    }
    
    public IEnumerator MovePortal() {
        
        while(_strCarutune) {
            _Portal.transform.position =
                Vector2.MoveTowards(_Portal.transform.position, 
                                    _Player.transform.position,
                                    Time.deltaTime * speedPortal);
            yield return null;
        }
    }
    
    private void MovingPortal() {
        _Portal.transform.position =
            Vector2.MoveTowards(_Portal.transform.position, 
                                _Player.transform.position,
                                Time.deltaTime * speedPortal);
    }

    public void EnemyEyes() {
        Vector2      shootRay = _ShootRay.transform.position;
        RaycastHit2D ray      = Physics2D.Raycast(shootRay, Vector2.left, 100);

        if(ray.collider != null) {
            print(ray.transform.gameObject.name);
            
        }
    }
}